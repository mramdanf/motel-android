## API DOCS ##
### Login ###
* URL: https://server.com/android_api/auth/login
* Method: POST(reg_id)
* Auth: Basic Auth
* Response
```json
{
    "status": true,
    "user": {
        "id": "1",
        "name": "Team 1",
        "username": "team1",
        "password": "123",
        "no_hp": "082333444555",
        "hak_akses": "team1"
    }
}
```
