package com.motel.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.motel.R;
import com.motel.model.Pemriksaan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ramdan Firdaus on 16/3/2017.
 */

public class HistoryTOListAdapter extends RecyclerView.Adapter<HistoryTOListAdapter.RecViewHolder> {

    private List<Pemriksaan> pemriksaanList;

    public class RecViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.his_item_id_pelanggan) TextView tvIdPelanggan;
        @BindView(R.id.his_item_tgl_periksa) TextView tvTglPeriksa;
        @BindView(R.id.his_item_nama_pemeriksa) TextView tvPemeriksa;
        @BindView(R.id.his_item_error_kwh) TextView tvErrorKwh;
        @BindView(R.id.his_item_stand_kwh) TextView tvStandKwh;
        public RecViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public HistoryTOListAdapter(List<Pemriksaan> pemriksaanList) {
        this.pemriksaanList = pemriksaanList;
    }

    @Override
    public RecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_pemeriksaan_list_item, parent, false);

        return new RecViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecViewHolder holder, int position) {
        Pemriksaan pemriksaan = pemriksaanList.get(position);
        holder.tvIdPelanggan.setText(pemriksaan.getIdPelanggan());
        holder.tvTglPeriksa.setText(pemriksaan.getTanggal());
        holder.tvErrorKwh.setText(pemriksaan.getErrorKwh());
        holder.tvStandKwh.setText(pemriksaan.getStand());
        holder.tvPemeriksa.setText(pemriksaan.getNamaPemeriska());
    }

    @Override
    public int getItemCount() {
        return pemriksaanList.size();
    }
}
