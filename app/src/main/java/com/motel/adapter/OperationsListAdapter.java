package com.motel.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.motel.R;
import com.motel.model.Operation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */

public class OperationsListAdapter extends RecyclerView.Adapter<OperationsListAdapter.RecViewHolder> {

    private List<Operation> operationList;
    private Context mContext;

    public class RecViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_id_pelanggan)
        TextView itemIdPelanggan;
        @BindView(R.id.item_nama_pelanggan)
        TextView itemNamaPelanggan;
        @BindView(R.id.item_add_pelanggan)
        TextView itemAddPelanggan;

        public RecViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public OperationsListAdapter(List<Operation> operationList, Context mContext) {
        this.operationList = operationList;
        this.mContext = mContext;
    }

    @Override
    public RecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.to_list_item, parent, false);
        return new RecViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecViewHolder holder, int position) {
        Operation operation = operationList.get(position);
        holder.itemIdPelanggan.setText(operation.getIdPelanggan());
        holder.itemNamaPelanggan.setText(operation.getNama());
        holder.itemAddPelanggan.setText(operation.getAlamat());
    }

    @Override
    public int getItemCount() {
        return operationList.size();
    }

    public void setFilter(List<Operation> operations) {
        operationList = new ArrayList<>();
        operationList.addAll(operations);
        notifyDataSetChanged();
    }
}
