package com.motel.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */
/*
* {
            "id": "1",
            "id_pelanggan": "514500098493",
            "id_user": "1",
            "nama": "SOEHADI TANOYO           ",
            "alamat": "DN NGROWO            ",
            "daya": "I2\/82500",
            "pembatas_daya": "3* 125",
            "ct_terpasang": "150\/5 A",
            "no_gardu": "T.",
            "merek": "LANDIG & GYR",
            "seri": "215444747",
            "tanggal": "2017-03-07",
            "latitude": "112.336718",
            "longitude": "-7.569275251"
        }
* */
public class Operation implements Parcelable {
    String id;
    @SerializedName("id_pelanggan")
    String idPelanggan;
    @SerializedName("id_user")
    String idUser;
    String nama;
    String alamat;
    String daya;
    @SerializedName("pembatas_daya")
    String pembatasDaya;
    @SerializedName("ct_terpasang")
    String ctTerpasang;
    @SerializedName("no_gardu")
    String noGardu;
    String merek;
    String seri;
    String tanggal;
    double latitude;
    double longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(String idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getPembatasDaya() {
        return pembatasDaya;
    }

    public void setPembatasDaya(String pembatasDaya) {
        this.pembatasDaya = pembatasDaya;
    }

    public String getCtTerpasang() {
        return ctTerpasang;
    }

    public void setCtTerpasang(String ctTerpasang) {
        this.ctTerpasang = ctTerpasang;
    }

    public String getNoGardu() {
        return noGardu;
    }

    public void setNoGardu(String noGardu) {
        this.noGardu = noGardu;
    }

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }

    public String getSeri() {
        return seri;
    }

    public void setSeri(String seri) {
        this.seri = seri;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.idPelanggan);
        dest.writeString(this.idUser);
        dest.writeString(this.nama);
        dest.writeString(this.alamat);
        dest.writeString(this.daya);
        dest.writeString(this.pembatasDaya);
        dest.writeString(this.ctTerpasang);
        dest.writeString(this.noGardu);
        dest.writeString(this.merek);
        dest.writeString(this.seri);
        dest.writeString(this.tanggal);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    public Operation() {
    }

    protected Operation(Parcel in) {
        this.id = in.readString();
        this.idPelanggan = in.readString();
        this.idUser = in.readString();
        this.nama = in.readString();
        this.alamat = in.readString();
        this.daya = in.readString();
        this.pembatasDaya = in.readString();
        this.ctTerpasang = in.readString();
        this.noGardu = in.readString();
        this.merek = in.readString();
        this.seri = in.readString();
        this.tanggal = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Creator<Operation> CREATOR = new Creator<Operation>() {
        @Override
        public Operation createFromParcel(Parcel source) {
            return new Operation(source);
        }

        @Override
        public Operation[] newArray(int size) {
            return new Operation[size];
        }
    };
}
