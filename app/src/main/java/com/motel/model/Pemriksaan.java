package com.motel.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 10/3/2017.
 */
/*
{
            "id": "1",
            "id_target_operasi": "11",
            "nama_pemeriksa": "ramdan",
            "stand_kwh": "stand",
            "error_kwh": "error",
            "keterangan": "ket",
            "photo": "e5d1b92d032457cbad55a7f4a06960d5.jpg",
            "tanggal": "2017-03-14"
        }
* */
public class Pemriksaan implements Parcelable {
    @SerializedName("nama_pemeriksa")
    String namaPemeriska;
    @SerializedName("stand_kwh")
    String stand;
    @SerializedName("error_kwh")
    String errorKwh;
    String keterangan;
    String tanggal;
    @SerializedName("id_target_operasi")
    String idTargetOperasi;
    String id;
    String photo;
    @SerializedName("id_pelanggan")
    String idPelanggan;
    String golongan;

    public String getNamaPemeriska() {
        return namaPemeriska;
    }

    public void setNamaPemeriska(String namaPemeriska) {
        this.namaPemeriska = namaPemeriska;
    }

    public String getStand() {
        return stand;
    }

    public void setStand(String stand) {
        this.stand = stand;
    }

    public String getErrorKwh() {
        return errorKwh;
    }

    public void setErrorKwh(String errorKwh) {
        this.errorKwh = errorKwh;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getIdTargetOperasi() {
        return idTargetOperasi;
    }

    public void setIdTargetOperasi(String idTargetOperasi) {
        this.idTargetOperasi = idTargetOperasi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(String idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.namaPemeriska);
        dest.writeString(this.stand);
        dest.writeString(this.errorKwh);
        dest.writeString(this.keterangan);
        dest.writeString(this.tanggal);
        dest.writeString(this.idTargetOperasi);
        dest.writeString(this.id);
        dest.writeString(this.photo);
        dest.writeString(this.idPelanggan);
        dest.writeString(this.golongan);
    }

    public Pemriksaan() {
    }

    protected Pemriksaan(Parcel in) {
        this.namaPemeriska = in.readString();
        this.stand = in.readString();
        this.errorKwh = in.readString();
        this.keterangan = in.readString();
        this.tanggal = in.readString();
        this.idTargetOperasi = in.readString();
        this.id = in.readString();
        this.photo = in.readString();
        this.idPelanggan = in.readString();
        this.golongan = in.readString();
    }

    public static final Creator<Pemriksaan> CREATOR = new Creator<Pemriksaan>() {
        @Override
        public Pemriksaan createFromParcel(Parcel source) {
            return new Pemriksaan(source);
        }

        @Override
        public Pemriksaan[] newArray(int size) {
            return new Pemriksaan[size];
        }
    };
}
