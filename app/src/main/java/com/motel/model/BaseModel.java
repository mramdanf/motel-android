package com.motel.model;

/**
 * Created by Ramdan Firdaus on 10/3/2017.
 */

public class BaseModel {
    boolean status;
    String msg;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
