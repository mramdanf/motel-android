package com.motel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.motel.fragment.FormPemriksaFragment;
import com.motel.fragment.PelangganFragment;
import com.motel.helper.ApiEndPoint;
import com.motel.helper.AppConst;
import com.motel.helper.Utility;
import com.motel.model.BaseModel;
import com.motel.model.Operation;
import com.motel.model.Pemriksaan;
import com.motel.model.User;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
// // TODO: 11/3/2017 itu instance retrofit pake user pass beneran lah
public class PelangganActivity extends AppCompatActivity implements
    PelangganFragment.PelangganFragmentInf,
    FormPemriksaFragment.FormPemriksaFragmentInf{

    private final String LOG_TAG = getClass().getSimpleName();
    private ProgressDialog pDialog;
    private Retrofit mRetrofitClient;
    private Context mContetx;
    private Uri mImgPemerikaanUri;
    private Pemriksaan mPemeriksaan;
    private String mImgPemeriksaanPath;
    private int mediaFlag;
    private User logedInUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelanggan);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        mContetx = PelangganActivity.this;

        logedInUser = Utility.getUserFromPrf(this);

        mRetrofitClient = Utility.getClient(
                logedInUser.getUsername(),
                logedInUser.getPassword(),
                this
        );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        if (i.hasExtra(AppConst.PARC_OPERATION)) {
            Operation operation = i.getParcelableExtra(AppConst.PARC_OPERATION);
            if (operation != null) {
                Bundle args = new Bundle();
                args.putParcelable(AppConst.PARC_OPERATION, operation);
                PelangganFragment f = new PelangganFragment();
                f.setArguments(args);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.pelanggal_fragment_container, f);
                transaction.commit();
            }
        }
    }

    @Override
    public void PelangganFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.TAG_ITEM_ID);
        switch (itemId) {
            case R.id.btn_periksa:
                FormPemriksaFragment f = new FormPemriksaFragment();
                f.setArguments(args);
                setUpFragment(f);
                break;
            default:
                break;
        }
    }

    @Override
    public void FromPemriksaFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.TAG_ITEM_ID);
        switch (itemId) {
            case R.id.btn_submit_pemeriksaan:
                mPemeriksaan = args.getParcelable(AppConst.PARC_PEMERIKSAAN);
                mImgPemerikaanUri = args.getParcelable(AppConst.TAG_IMG_URI);
                mediaFlag = args.getInt(AppConst.TAG_SOME_FLAG);
                submitPemeriksaanWithApi();
                break;
            case R.id.btn_back_form_pemeriksa:
                super.onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setUpFragment(Fragment f) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.pelanggal_fragment_container, f);
        String tag = fragmentTransaction.toString();
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    private void submitPemeriksaanWithApi() {
        pDialog.show();

        File imgFile;
        String mediaType;

        mediaType = "image/jpeg";
        imgFile = new File(mImgPemerikaanUri.getPath());

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(mediaType),
                        imgFile
                );
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("img_pemeriksaan", imgFile.getName(), requestFile);

        ApiEndPoint endPoint = mRetrofitClient.create(ApiEndPoint.class);
        Call<BaseModel> call = endPoint.submitPemeriksaanApi(
                RequestBody.create( okhttp3.MultipartBody.FORM, ""),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getNamaPemeriska()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getGolongan()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getIdTargetOperasi()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getStand()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getErrorKwh()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getKeterangan()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemeriksaan.getTanggal()),
                body

        );
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                pDialog.dismiss();
                if (response.code() == AppConst.HTTP_OK) {
                    Utility.displayShortToast(mContetx, response.body().getMsg());
                    finish();
                } else {
                    Utility.displayAlert(mContetx, "Error: " + response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                pDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(mContetx, t.getMessage());
                } else {
                    Utility.displayAlert(mContetx, "Terjadi error, error msg: " + t.getMessage());
                }
            }
        });
    }

}
