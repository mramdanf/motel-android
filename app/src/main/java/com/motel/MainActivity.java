package com.motel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.motel.apiresponse.HistoryPemeriksaanResponse;
import com.motel.apiresponse.OperationsResponse;
import com.motel.fragment.FormPemriksaFragment;
import com.motel.fragment.PemeriksaanFragment;
import com.motel.fragment.TOHistoryFragment;
import com.motel.helper.ApiEndPoint;
import com.motel.helper.AppConst;
import com.motel.helper.Utility;
import com.motel.model.BaseModel;
import com.motel.model.Operation;
import com.motel.model.Pemriksaan;
import com.motel.model.User;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        PemeriksaanFragment.PemeriksaanFragmentInf,
        TOHistoryFragment.TOHistoryFragmentInf,
        FormPemriksaFragment.FormPemriksaFragmentInf{

    private Toolbar toolbar;

    private ProgressDialog pDialog;
    private OperationsResponse operationsResponse;
    private Pemriksaan mPemriksaan;
    private Uri mImgPemerikaanUri;
    private int mediaFlag;

    private Retrofit mRetrofitClient;
    private User logedInUser;
    private Context mContext;
    private TextView tvCustFullname;
    private final String LOG_TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait..");
        pDialog.setCancelable(false);


        mContext = MainActivity.this.getApplicationContext();
        logedInUser = Utility.getUserFromPrf(this);

        mRetrofitClient = Utility.getClient(
                logedInUser.getUsername(),
                logedInUser.getPassword(),
                this
        );

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        tvCustFullname = (TextView) header.findViewById(R.id.tv_nav_username);
        tvCustFullname.setText("Welcome, "+logedInUser.getName());

        navigationView.setCheckedItem(R.id.nav_item_to);
        getTOFromApi();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int navItemId = item.getItemId();
        switch (navItemId){
            case R.id.nav_item_to:
                getTOFromApi();
                break;
            case R.id.nav_item_history:
                getPemeriksaanFromApi();
                break;
            case R.id.nav_item_logout:
                Utility.removeUserFromPrf(this);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;
            default:
                break;

        }

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void PemeriksaanFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.TAG_ITEM_ID);
        switch (itemId) {
            case AppConst.ITEM_RECY_CLICK:
                Operation operation = args.getParcelable(AppConst.PARC_OPERATION);
                Intent i = new Intent(MainActivity.this, PelangganActivity.class);
                i.putExtra(AppConst.PARC_OPERATION, operation);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    @Override
    public void TOHistoryFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.TAG_ITEM_ID);

        switch (itemId) {
            case AppConst.ITEM_RECY_CLICK:
                FormPemriksaFragment f = new FormPemriksaFragment();
                args.putInt(AppConst.TAG_COME_FROM_FLAG, AppConst.TO_HISTORY_FRAGMENT_ID);
                f.setArguments(args);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, f);
                String tag = f.getTag();
                fragmentTransaction.addToBackStack(tag);
                fragmentTransaction.commit();
                break;
            case AppConst.ITEM_DELETE_PEMERIKSAAN:
                mPemriksaan = args.getParcelable(AppConst.PARC_PEMERIKSAAN);
                deletePemeriksaanWithApi();
                break;
        }
    }

    @Override
    public void FromPemriksaFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.TAG_ITEM_ID);
        switch (itemId) {
            case R.id.btn_submit_pemeriksaan:
                mPemriksaan = args.getParcelable(AppConst.PARC_PEMERIKSAAN);
                mImgPemerikaanUri = args.getParcelable(AppConst.TAG_IMG_URI);
                updatePemeriksaanWithApi();
                break;
            case R.id.btn_back_form_pemeriksa:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getSupportFragmentManager().popBackStack();
                else
                    super.onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            getSupportFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "on restart invokded");
        getTOFromApi();
    }

    private void setUpFragment(Fragment f) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, f);
        fragmentTransaction.commit();
    }

    private void getTOFromApi() {
        pDialog.show();
        ApiEndPoint endPoint = mRetrofitClient.create(ApiEndPoint.class);
        Call<OperationsResponse> call = endPoint.getOperations(logedInUser.getId());
        call.enqueue(new Callback<OperationsResponse>() {
            @Override
            public void onResponse(Call<OperationsResponse> call, Response<OperationsResponse> response) {
                pDialog.dismiss();
                if (response.code() == AppConst.HTTP_OK) {
                    if (response.body().isStatus()) {
                        operationsResponse = response.body();

                        Bundle args = new Bundle();
                        args.putParcelable(AppConst.PARC_OPERATION_RESP, operationsResponse);
                        PemeriksaanFragment f = new PemeriksaanFragment();
                        f.setArguments(args);
                        setUpFragment(f);
                    }
                }
            }

            @Override
            public void onFailure(Call<OperationsResponse> call, Throwable t) {
                pDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(MainActivity.this, t.getMessage());
                }else {
                    Utility.displayAlert(MainActivity.this, "Terjadi error, error msg: " + t.getMessage());
                }
            }
        });
    }

    private void getPemeriksaanFromApi() {
        pDialog.show();
        ApiEndPoint endPoint = mRetrofitClient.create(ApiEndPoint.class);
        Call<HistoryPemeriksaanResponse> call = endPoint.getPemeriksaan(logedInUser.getId());
        call.enqueue(new Callback<HistoryPemeriksaanResponse>() {
            @Override
            public void onResponse(Call<HistoryPemeriksaanResponse> call, Response<HistoryPemeriksaanResponse> response) {
                pDialog.dismiss();
                Bundle args = new Bundle();
                if (response.code() == AppConst.HTTP_OK) {
                    if (response.body().isStatus()) {
                        args.putParcelable(AppConst.PARC_HISTORY_TO_RESP, response.body());
                    }
                }
                TOHistoryFragment f = new TOHistoryFragment();
                f.setArguments(args);
                setUpFragment(f);
            }

            @Override
            public void onFailure(Call<HistoryPemeriksaanResponse> call, Throwable t) {
                pDialog.dismiss();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(MainActivity.this, t.getMessage());
                } else {
                    Utility.displayAlert(MainActivity.this, "Terjadi error, error msg: " + t.getMessage());
                }
            }
        });
    }

    private void deletePemeriksaanWithApi() {
        pDialog.show();
        if (mPemriksaan != null) {
            ApiEndPoint endPoint = mRetrofitClient.create(ApiEndPoint.class);
            Call<BaseModel> call = endPoint.deletePemeriksaanApi(mPemriksaan.getId());
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    pDialog.dismiss();
                    if (response.code() == AppConst.HTTP_OK){
                        if (response.body().isStatus()) {
                           getPemeriksaanFromApi();
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    pDialog.dismiss();
                    if (t instanceof IOException) {
                        Utility.displayNoIntAlert(MainActivity.this, t.getMessage());
                    }else {
                        Utility.displayAlert(MainActivity.this, "Terjadi error, error msg: " + t.getMessage());
                    }
                }
            });
        }

    }

    private void updatePemeriksaanWithApi() {
        pDialog.show();

        File imgFile;
        String mediaType;

        mediaType = "image/jpeg";
        // for check when user not change image
        if (mImgPemerikaanUri != null) {
            // image changed
            Log.d(LOG_TAG, "changed img path: " + mImgPemerikaanUri.getPath());
            imgFile = new File(mImgPemerikaanUri.getPath());

        }  else {
            // image unchanged
            Log.d(LOG_TAG, "unchanged img path: " + AppConst.LOCAL_FOLDER + mPemriksaan.getPhoto());
            imgFile = new File(AppConst.LOCAL_FOLDER + mPemriksaan.getPhoto());
        }


        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(mediaType),
                        imgFile
                );
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("img_pemeriksaan", imgFile.getName(), requestFile);

        ApiEndPoint endPoint = mRetrofitClient.create(ApiEndPoint.class);
        Call<BaseModel> call = endPoint.submitPemeriksaanApi(
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getId()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getNamaPemeriska()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getGolongan()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getIdTargetOperasi()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getStand()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getErrorKwh()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getKeterangan()),
                RequestBody.create( okhttp3.MultipartBody.FORM, mPemriksaan.getTanggal()),
                body

        );
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                pDialog.dismiss();
                if (response.code() == AppConst.HTTP_OK) {
                    Utility.displayShortToast(mContext, response.body().getMsg());
                    getPemeriksaanFromApi();
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                pDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(mContext, t.getMessage());
                } else {
                    Utility.displayAlert(MainActivity.this, "Terjadi error, error msg: " + t.getMessage());
                }
            }
        });
    }

}
