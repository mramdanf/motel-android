package com.motel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;

import com.motel.helper.Utility;
import com.motel.model.User;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        int SPLASH_DISPLAY_LENGTH = 3000;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                User user = Utility.getUserFromPrf(SplashActivity.this);
                if (TextUtils.equals("", user.getUsername())) {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
