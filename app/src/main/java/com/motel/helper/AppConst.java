package com.motel.helper;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */

public interface AppConst {

    //String BASE_URL =  "http://10.99.226.135/motel/android_api/";
    String BASE_URL =  "/motel/android_api/";
    String BASE_URL_IDHOSTINGER = "http://monitoringp2tl.esy.es/android_api/";
    String LOCAL_FOLDER = "/storage/emulated/0/Pictures/Motel/";

    int HTTP_OK = 200;
    int HTTP_UNAUTHORIZED = 401;

    String PARC_USER = "parc-user";
    String PARC_OPERATION_RESP = "parc-operation-resp";
    String PARC_OPERATION = "parc-operation";
    String PARC_PEMERIKSAAN = "parc-pemeriksaan";
    String PARC_BITMAP_PEMERIKSAAN = "pacr-bitmap-pemeriksaan";
    String PARC_HISTORY_TO_RESP = "history-to-resp";

    String TAG_ITEM_ID = "item-id";
    String TAG_TO_ID = "to-id";
    String TAG_IMG_PATH = "img-path";
    String TAG_IMG_URI = "img-uri";
    String TAG_SOME_FLAG = "some-flag";
    String TAG_COME_FROM_FLAG = "come-from-flag";

    String PRF_APP = "prf-app";
    String PRF_KEY_IMGURI = "prf-key-imguri";
    String PRF_KEY_USERDATASET = "prf-key-userdataset";
    String PRF_USERNAME = "prf-username";
    String PRF_PASSWORD = "prf-pass";
    String PRF_KEY_USERID = "prf-key-userid";
    String PRF_KEY_FULLNAME = "prf-key-fullname";
    String PRF_KEY_IP = "prf-key-ip";


    int ITEM_RECY_CLICK = 111;
    int ITEM_DELETE_PEMERIKSAAN = 112;
    int TAG_ACTION_OPEN_CAMERA = 0;
    int TAG_ACTION_OPEN_GALLERY = 1;
    int MEDIA_FLAG_GALLERY = 4;
    int MEDIA_FLAG_CAMERA = 5;
    int TO_HISTORY_FRAGMENT_ID = 6;
    int PELANGGAN_FRAGMENT_ID = 7;
}
