package com.motel.helper;

import com.motel.apiresponse.HistoryPemeriksaanResponse;
import com.motel.apiresponse.LoginResponse;
import com.motel.apiresponse.OperationsResponse;
import com.motel.model.BaseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */

public interface ApiEndPoint {

    @POST("auth/login")
    Call<LoginResponse> checkLoginApi();

    @GET("target_operasi")
    Call<OperationsResponse> getOperations(
            @Query("id_user") String idUser
    );

    @Multipart
    @POST("pemeriksaan/modify")
    Call<BaseModel> submitPemeriksaanApi(
            @Part("id") RequestBody pemeriksaanId,
            @Part("nama_pemeriksa") RequestBody namaPemeriksa,
            @Part("golongan") RequestBody golongan,
            @Part("id_target_operasi") RequestBody idTargetOperasi,
            @Part("stand_kwh") RequestBody standKwh,
            @Part("error_kwh") RequestBody errorKwh,
            @Part("keterangan") RequestBody keterangan,
            @Part("tanggal") RequestBody tanggal,
            @Part MultipartBody.Part file
    );

    @GET("pemeriksaan")
    Call<HistoryPemeriksaanResponse> getPemeriksaan(
            @Query("id_user") String idUser
    );

    @FormUrlEncoded
    @POST("pemeriksaan/delete")
    Call<BaseModel> deletePemeriksaanApi(
            @Field("id") String id
    );
}
