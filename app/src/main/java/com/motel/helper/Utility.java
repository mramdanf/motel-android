package com.motel.helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.motel.R;
import com.motel.model.User;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.PUT;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */

public class Utility {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static Retrofit getClient(String customerId, String password, Context c){

        Retrofit retrofit;

        retrofit = null;

        if(customerId != null && password != null) {
            String credentials = customerId + ":" + password;

            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addNetworkInterceptor(httpLoggingInterceptor);

            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.connectTimeout(60, TimeUnit.SECONDS);

            OkHttpClient client = builder.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConst.BASE_URL_IDHOSTINGER)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public static void displayAlert(final Context c, String msg){

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(c);

        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = layoutInflater.inflate(R.layout.custom_alert_dialog, null);
        TextView tvCustomAlert = (TextView) dialogView.findViewById(R.id.tv_custom_alert);
        tvCustomAlert.setText(msg);

        builder
                .setView(dialogView)
                .setTitle(c.getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                dialog.cancel();
                            }
                        })
                .show();

    }

    public static void displayNoIntAlert(final Context c, String error) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(c);

        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = layoutInflater.inflate(R.layout.custom_alert_dialog, null);
        TextView tvCustomAlert = (TextView) dialogView.findViewById(R.id.tv_custom_alert);
        tvCustomAlert.setText(c.getString(R.string.text_network_error) + ", " + error + ". curr ip: " + Utility.getIpFromPrf(c));

        builder
                .setView(dialogView)
                .setTitle(c.getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                dialog.cancel();
                            }
                        })

                .show();
    }

    public static void displayShortToast(Context c, String msg) {
        Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static void saveUserOnPrf(Context c, User user) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prf.edit();
        editor.putString(AppConst.PRF_USERNAME, user.getUsername());
        editor.putString(AppConst.PRF_PASSWORD, user.getPassword());
        editor.putString(AppConst.PRF_KEY_USERID, user.getId());
        editor.putString(AppConst.PRF_KEY_FULLNAME, user.getName());
        editor.apply();
    }

    public static User getUserFromPrf(Context c) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_APP, Context.MODE_PRIVATE);
        User user = new User();
        user.setUsername(prf.getString(AppConst.PRF_USERNAME, ""));
        user.setPassword(prf.getString(AppConst.PRF_PASSWORD, ""));
        user.setId(prf.getString(AppConst.PRF_KEY_USERID, ""));
        user.setName(prf.getString(AppConst.PRF_KEY_FULLNAME, ""));
        return user;
    }

    public static void removeUserFromPrf(Context c) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prf.edit();
        editor.putString(AppConst.PRF_USERNAME, null);
        editor.putString(AppConst.PRF_PASSWORD, null);
        editor.apply();
    }

    public static String getRealPathFromURI(Uri contentURI, Activity context) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = context.managedQuery(contentURI, projection, null,
                null, null);
        if (cursor == null)
            return null;
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        if (cursor.moveToFirst()) {
            String s = cursor.getString(column_index);
            return s;
        }
        return null;
    }

    public static String getFileName(Context c, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static void saveIpToPrf(String IP, Context c) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prf.edit();
        editor.putString(AppConst.PRF_KEY_IP, IP);
        editor.apply();
    }

    public static String getIpFromPrf(Context c) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_APP, Context.MODE_PRIVATE);
        return prf.getString(AppConst.PRF_KEY_IP, "");
    }

    public static void showIpSettingDialog(final Context c) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(c);

        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = layoutInflater.inflate(R.layout.ip_setting_dialog, null);
        TextView tvCurrentIp = (TextView) dialogView.findViewById(R.id.tv_current_ip);
        tvCurrentIp.setText("IP Saat ini: "+Utility.getIpFromPrf(c));

        builder
                .setView(dialogView)
                .setTitle(c.getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton(c.getString(R.string.text_save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                EditText etNewIp = (EditText) dialogView.findViewById(R.id.tv_new_ip);
                                Utility.saveIpToPrf(etNewIp.getText().toString(), c);
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(c.getString(R.string.text_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .show();
    }
}
