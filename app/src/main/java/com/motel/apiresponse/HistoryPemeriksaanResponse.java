package com.motel.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.motel.model.Pemriksaan;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 14/3/2017.
 */

public class HistoryPemeriksaanResponse implements Parcelable{
    boolean status;
    String msg;
    @SerializedName("pemeriksaan")
    List<Pemriksaan> pemriksaanList;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Pemriksaan> getPemriksaanList() {
        return pemriksaanList;
    }

    public void setPemriksaanList(List<Pemriksaan> pemriksaanList) {
        this.pemriksaanList = pemriksaanList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.msg);
        dest.writeTypedList(this.pemriksaanList);
    }

    public HistoryPemeriksaanResponse() {
    }

    protected HistoryPemeriksaanResponse(Parcel in) {
        this.status = in.readByte() != 0;
        this.msg = in.readString();
        this.pemriksaanList = in.createTypedArrayList(Pemriksaan.CREATOR);
    }

    public static final Creator<HistoryPemeriksaanResponse> CREATOR = new Creator<HistoryPemeriksaanResponse>() {
        @Override
        public HistoryPemeriksaanResponse createFromParcel(Parcel source) {
            return new HistoryPemeriksaanResponse(source);
        }

        @Override
        public HistoryPemeriksaanResponse[] newArray(int size) {
            return new HistoryPemeriksaanResponse[size];
        }
    };
}
