package com.motel.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.motel.model.User;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */

public class LoginResponse implements Parcelable {
    String msg;
    boolean status;
    User user;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.msg);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.user, flags);
    }

    public LoginResponse() {
    }

    protected LoginResponse(Parcel in) {
        this.msg = in.readString();
        this.status = in.readByte() != 0;
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel source) {
            return new LoginResponse(source);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };
}
