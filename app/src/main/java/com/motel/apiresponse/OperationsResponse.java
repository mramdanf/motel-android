package com.motel.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.motel.model.Operation;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 7/3/2017.
 */

public class OperationsResponse implements Parcelable {
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Operation> getOperationList() {
        return operationList;
    }

    public void setOperationList(List<Operation> operationList) {
        this.operationList = operationList;
    }

    boolean status;
    String msg;
    @SerializedName("operations")
    List<Operation> operationList;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.msg);
        dest.writeTypedList(this.operationList);
    }

    public OperationsResponse() {
    }

    protected OperationsResponse(Parcel in) {
        this.status = in.readByte() != 0;
        this.msg = in.readString();
        this.operationList = in.createTypedArrayList(Operation.CREATOR);
    }

    public static final Creator<OperationsResponse> CREATOR = new Creator<OperationsResponse>() {
        @Override
        public OperationsResponse createFromParcel(Parcel source) {
            return new OperationsResponse(source);
        }

        @Override
        public OperationsResponse[] newArray(int size) {
            return new OperationsResponse[size];
        }
    };
}
