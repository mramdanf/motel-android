package com.motel.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.icu.text.LocaleDisplayNames;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.motel.R;
import com.motel.helper.AppConst;
import com.motel.helper.Utility;
import com.motel.model.Pemriksaan;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
// TODO: 28/3/2017 kasih alert pas ada yang belum lengkap ngisi form pemeriksaan
// TODO: 28/3/2017 print error ketika error submit pemeriksaan
public class FormPemriksaFragment extends Fragment implements
        DatePickerDialog.OnDateSetListener{

    @BindView(R.id.tv_pilih_tanggal)
    TextView tvPilihTgl;
    @BindView(R.id.tv_nama_pemeriksa)
    EditText tvNamaPemeriksa;
    @BindView(R.id.tv_stand)
    EditText tvStand;
    @BindView(R.id.tv_error_kwh)
    EditText tvError;
    @BindView(R.id.tv_keterangan)
    EditText tvKet;
    @BindView(R.id.img_pemeriksaan)
    ImageView imgPemeriksaan;
    @BindView(R.id.btn_submit_pemeriksaan)
    Button btnSubmitPemeriksaan;
    @BindView(R.id.spinner_golongan)
    Spinner spinnerGolongan;

    private FormPemriksaFragmentInf mCallback;
    private String toId;
    private String mPemeriksaanId;
    private Uri mImgPemerikasaanUri;

    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int mediaFlag;
    private Pemriksaan mPemriksaan;
    private final String LOG_TAG = getClass().getSimpleName();
    private final int IMG_PEMERIKSAAN_W = 1000;
    private final int IMG_PEMERIKSAAN_H = 600;




    public FormPemriksaFragment() {
        // Required empty public constructor
    }


    public interface FormPemriksaFragmentInf {
        void FromPemriksaFragmentClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_form_pemriksa, container, false);

        getActivity().setTitle("Form Pemeriksaan");

        ButterKnife.bind(this, rootView);


        Bundle args = getArguments();
        if (args != null) {
            toId = args.getString(AppConst.TAG_TO_ID);
            int comeFromFlag = args.getInt(AppConst.TAG_COME_FROM_FLAG);

            if (comeFromFlag == AppConst.TO_HISTORY_FRAGMENT_ID){
                mPemriksaan = args.getParcelable(AppConst.PARC_PEMERIKSAAN);
                btnSubmitPemeriksaan.setText("Update");
                toId = mPemriksaan.getIdTargetOperasi();
                populateEditText();
            }
        }

        return rootView;
    }

    @OnClick(R.id.tv_pilih_tanggal) public void clickPilihTgl() {
        displayDatePicker();
    }

    @OnClick(R.id.btn_submit_pemeriksaan) public void submitPemriksaan(View v) {
        Bundle args = new Bundle();
        Pemriksaan pemriksaan = new Pemriksaan();

        if (mPemriksaan != null) {
            pemriksaan.setPhoto(mPemriksaan.getPhoto());
            pemriksaan.setId(mPemriksaan.getId());
        }


        pemriksaan.setIdTargetOperasi(toId);
        pemriksaan.setNamaPemeriska(tvNamaPemeriksa.getText().toString());
        pemriksaan.setErrorKwh(tvError.getText().toString());
        pemriksaan.setKeterangan(tvKet.getText().toString());
        pemriksaan.setStand(tvStand.getText().toString());

        if (tvPilihTgl.getText().toString().equals(getString(R.string.choose_date))) {

            Utility.displayAlert(getActivity(), "Anda belum milih tanggal.");
            return;

        } else pemriksaan.setTanggal(tvPilihTgl.getText().toString());


        pemriksaan.setGolongan(spinnerGolongan.getSelectedItem().toString());

        args.putParcelable(AppConst.PARC_PEMERIKSAAN, pemriksaan);
        args.putInt(AppConst.TAG_ITEM_ID, v.getId());
        args.putParcelable(AppConst.TAG_IMG_URI, mImgPemerikasaanUri);
        args.putInt(AppConst.TAG_SOME_FLAG, mediaFlag);

        if (mImgPemerikasaanUri == null) {
            Utility.displayAlert(getActivity(), "Anda belum memgambil foto.");
            return;
        }


        mCallback.FromPemriksaFragmentClickListener(args);
    }

    @OnClick(R.id.img_pemeriksaan) public void showImagePicker() {
        selectImageDialogList();
    }

    @OnClick(R.id.btn_back_form_pemeriksa) public void btnBackFormPemeriksa(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.TAG_ITEM_ID, v.getId());
        mCallback.FromPemriksaFragmentClickListener(args);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
        tvPilihTgl.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (FormPemriksaFragmentInf) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context + " Activity must implement FormPemriksaFragmentInf");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        Picasso.with(getActivity()).cancelRequest(targetForGetImgLocaly);
        Picasso.with(getActivity()).cancelRequest(targetForGetImgUrl);
        super.onDestroy();
    }

    private void selectImageDialogList() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask ="CChoose from Gallery";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        mediaFlag = AppConst.MEDIA_FLAG_GALLERY;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent() {
        mediaFlag = AppConst.MEDIA_FLAG_CAMERA;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mImgPemerikasaanUri = getOutputMediaFileUri();
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImgPemerikasaanUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void onCaptureImageResult() {
        if (mImgPemerikasaanUri != null) {
            getImgFromFile(Utility.getFileName(getActivity(), mImgPemerikasaanUri));
        } else {
            Log.d(LOG_TAG, "mImgPemerikasaanUri IS null");
        }

    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                File outPutFile = getOutputMediaFile();

                //File sourceFile = new File(data.getData().getPath());
                File sourceFile = new File(Utility.getRealPathFromURI(data.getData(), getActivity()));

                copyFile(sourceFile, outPutFile);

                mImgPemerikasaanUri = Uri.fromFile(outPutFile);

                getImgFromFile(Utility.getFileName(getActivity(), mImgPemerikasaanUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void displayDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Motel");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("formpemeriksaan", "Oops! Failed create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private void populateEditText() {
        if (mPemriksaan != null) {
            tvPilihTgl.setText(mPemriksaan.getTanggal());
            tvNamaPemeriksa.setText(mPemriksaan.getNamaPemeriska());
            tvError.setText(mPemriksaan.getErrorKwh());
            tvStand.setText(mPemriksaan.getStand());
            tvKet.setText(mPemriksaan.getKeterangan());

            String compareValue = mPemriksaan.getGolongan();
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.string_golongan, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerGolongan.setAdapter(adapter);
            if (!compareValue.equals(null)) {
                int spinnerPosition = adapter.getPosition(compareValue);
                spinnerGolongan.setSelection(spinnerPosition);
            }

            getImgFromFile(mPemriksaan.getPhoto());
        } else {
            Log.e(LOG_TAG, "objek pemeriksaan was null");
        }
    }

    private void getImgFromUrl(){
        if (mPemriksaan != null) {
            Picasso
                    .with(getActivity())
                    .load("http://"+ Utility.getIpFromPrf(getActivity()) +"/motel/assets/img/pemeriksaan/"
                            + mPemriksaan.getPhoto())
                    .resize(IMG_PEMERIKSAAN_W, IMG_PEMERIKSAAN_H)
                    .centerInside()
                    .into(targetForGetImgUrl);
        }
    }

    private void getImgFromFile(String imgName){
        Log.d(LOG_TAG, "search img locally in: " + AppConst.LOCAL_FOLDER + imgName);
        Picasso
                .with(getActivity())
                .load("file:///" + AppConst.LOCAL_FOLDER + imgName)
                .resize(IMG_PEMERIKSAAN_W, IMG_PEMERIKSAAN_H)
                .centerInside()
                .into(targetForGetImgLocaly);
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            Utility.displayAlert(getActivity(), LOG_TAG + ", error: source file dosen't exist.");
            return;
        }

        FileChannel source;
        FileChannel destination;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }

    private Target targetForGetImgLocaly = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.d(LOG_TAG, "imgae found locally");
            imgPemeriksaan.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.d(LOG_TAG, "imgae not found locally, will start downloadImgUrl");
            getImgFromUrl();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    private Target targetForGetImgUrl = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    File mediaStorageDir = new File(
                            Environment
                                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            "Motel");

                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {
                            Log.d("formpemeriksaan", "Oops! Failed create directory");
                        }
                    }
                    File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                            + mPemriksaan.getPhoto());
                    try {
                        mediaFile.createNewFile();
                        FileOutputStream outputStream = new FileOutputStream(mediaFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, outputStream);
                        outputStream.flush();
                        outputStream.close();
                        Log.d(LOG_TAG, "download image from url success and saved locally");
                    } catch (IOException e){
                        Log.e("IOException", e.getLocalizedMessage());
                    }

                }
            }).start();

            imgPemeriksaan.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };
}
