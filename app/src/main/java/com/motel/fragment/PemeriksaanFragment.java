package com.motel.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.motel.R;
import com.motel.adapter.OperationsListAdapter;
import com.motel.apiresponse.OperationsResponse;
import com.motel.helper.AppConst;
import com.motel.helper.RecyclerItemClickListener;
import com.motel.model.Operation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PemeriksaanFragment extends Fragment implements
        RecyclerItemClickListener.OnItemClickListener,
        SearchView.OnQueryTextListener{

    private Context mContext;
    private OperationsListAdapter operationsListAdapter;
    private List<Operation> operationList;
    private PemeriksaanFragmentInf mCallback;
    SearchView searchView;

    @BindView(R.id.recycler_to) RecyclerView recyclerViewTO;



    public PemeriksaanFragment() {
        // Required empty public constructor
    }

    public interface PemeriksaanFragmentInf {
        void PemeriksaanFragmentClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pemeriksaan, container, false);

        ButterKnife.bind(this, rootView);

        mContext = getActivity();

        getActivity().setTitle("Data TO");

        Bundle args = getArguments();
        if (args != null) {
            OperationsResponse operationsResponse = args.getParcelable(AppConst.PARC_OPERATION_RESP);
            if (operationsResponse != null) {
                operationList = operationsResponse.getOperationList();
                operationsListAdapter = new OperationsListAdapter(operationList, mContext);

                setUpRecyclerview();
            }
        }


        return rootView;
    }

    @Override
    public void onItemClick(View childView, int position) {
        Bundle args = new Bundle();
        args.putInt(AppConst.TAG_ITEM_ID, AppConst.ITEM_RECY_CLICK);
        args.putParcelable(AppConst.PARC_OPERATION, operationList.get(position));
        mCallback.PemeriksaanFragmentClickListener(args);
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (PemeriksaanFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " activity must implemnt PemeriksaanFragmentInf");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        operationsListAdapter.setFilter(operationList);

                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        return true;
                    }
                });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        doSearch();
        return true;
    }

    private List<Operation> filter(List<Operation> models, String query) {
        query = query.toLowerCase();

        final List<Operation> filteredModelList = new ArrayList<>();
        for (Operation model : models) {
            final String nama = model.getNama().toLowerCase();
            final String idPelanggan = model.getIdPelanggan().toLowerCase();
            if (nama.contains(query) || idPelanggan.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private void doSearch(){
        String query = searchView.getQuery().toString();
        if ((query != null) || !(query.equals(""))){
            final List<Operation> filteredModelList = filter(operationList, query);
            operationsListAdapter.setFilter(filteredModelList);
        }
    }

    private void setUpRecyclerview() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerViewTO.setLayoutManager(layoutManager);
        recyclerViewTO.setItemAnimator(new DefaultItemAnimator());
        recyclerViewTO.addOnItemTouchListener(new RecyclerItemClickListener(mContext, this));
        recyclerViewTO.setAdapter(operationsListAdapter);
    }

}
