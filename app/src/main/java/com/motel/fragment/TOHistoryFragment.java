package com.motel.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motel.R;
import com.motel.adapter.HistoryTOListAdapter;
import com.motel.apiresponse.HistoryPemeriksaanResponse;
import com.motel.helper.AppConst;
import com.motel.helper.RecyclerItemClickListener;
import com.motel.model.Pemriksaan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TOHistoryFragment extends Fragment implements
    RecyclerItemClickListener.OnItemClickListener{

    private final String LOG_TAG = getClass().getSimpleName();
    private HistoryTOListAdapter historyTOListAdapter;
    private List<Pemriksaan> pemriksaanList;
    private TOHistoryFragmentInf mCallback;

    @BindView(R.id.recycler_to_history)
    RecyclerView recToHistory;


    public TOHistoryFragment() {
        // Required empty public constructor
    }

    public interface TOHistoryFragmentInf {
        void TOHistoryFragmentClickListener(Bundle args);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tohistory, container, false);

        getActivity().setTitle("History Pemeriksaan");

        ButterKnife.bind(this, rootView);

        Bundle args = getArguments();
        if (args != null) {
            HistoryPemeriksaanResponse res = args.getParcelable(AppConst.PARC_HISTORY_TO_RESP);
            if (res != null) {
                Log.d(LOG_TAG, "res size: " + res.getPemriksaanList().size());
                pemriksaanList = res.getPemriksaanList();
                historyTOListAdapter = new HistoryTOListAdapter(pemriksaanList);
            }
            setUpRecyclerview();

        }

        return rootView;
    }

    @Override
    public void onItemClick(View childView, int position) {
        Bundle args = new Bundle();
        args.putInt(AppConst.TAG_ITEM_ID, AppConst.ITEM_RECY_CLICK);
        args.putParcelable(AppConst.PARC_PEMERIKSAAN, pemriksaanList.get(position));
        mCallback.TOHistoryFragmentClickListener(args);
    }

    @Override
    public void onItemLongPress(View childView, int position) {
        showDialogList(position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (TOHistoryFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " Activity must implement TOHistoryFragmentInf");
        }
    }

    private void setUpRecyclerview() {
       RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recToHistory.setLayoutManager(manager);
        recToHistory.setItemAnimator(new DefaultItemAnimator());
        recToHistory.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), this));
        recToHistory.setAdapter(historyTOListAdapter);
    }

    private void showDialogList(final int position){
        CharSequence[] chooices = {"View detail", "Delete Pemeriksaan"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle(getString(R.string.app_name))
                .setItems(chooices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle args = new Bundle();
                        switch (which) {
                            case 0:
                                args.putInt(AppConst.TAG_ITEM_ID, AppConst.ITEM_RECY_CLICK);
                                args.putParcelable(AppConst.PARC_PEMERIKSAAN, pemriksaanList.get(position));
                                mCallback.TOHistoryFragmentClickListener(args);
                                break;
                            case 1:
                                AlertDialog.Builder verifyDelete = new AlertDialog.Builder(getActivity());
                                verifyDelete
                                        .setTitle(getString(R.string.app_name))
                                        .setMessage(getString(R.string.text_are_you_sure))
                                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Bundle argsDelete = new Bundle();
                                                argsDelete.putInt(AppConst.TAG_ITEM_ID, AppConst.ITEM_DELETE_PEMERIKSAAN);
                                                argsDelete.putParcelable(AppConst.PARC_PEMERIKSAAN, pemriksaanList.get(position));
                                                mCallback.TOHistoryFragmentClickListener(argsDelete);
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }

                                        }).show();
                                break;
                        }
                    }
                }).show();
    }



}
