package com.motel.fragment;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.motel.R;
import com.motel.helper.AppConst;
import com.motel.model.Operation;

import org.w3c.dom.Text;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PelangganFragment extends Fragment {

    @BindView(R.id.tv_id_pelanggan)
    TextView tvIdPelanggan;
    @BindView(R.id.tv_nama_pelanggan)
    TextView tvNamaPelanggan;
    @BindView(R.id.tv_alamat_pelanggan)
    TextView tvAlamatPelanggan;
    @BindView(R.id.tv_daya_pelanggan)
    TextView tvDaya;
    @BindView(R.id.tv_ct_pelanggan)
    TextView tvCt;
    @BindView(R.id.tv_gardu_pelanggan)
    TextView tvGardu;
    @BindView(R.id.tv_merek_pelanggan)
    TextView tvMerek;
    @BindView(R.id.tv_seri_pelanggan)
    TextView tvSeri;


    private Operation operation;
    private PelangganFragmentInf mCallback;
    private final String LOG_TAG = getClass().getSimpleName();



    public PelangganFragment() {
        // Required empty public constructor
    }

    public interface PelangganFragmentInf {
        void PelangganFragmentClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView = inflater.inflate(R.layout.fragment_pelanggan, container, false);

        ButterKnife.bind(this, rooView);

        getActivity().setTitle("Detail Pelanggan");

        Bundle args = getArguments();
        if (args != null) {
            operation = args.getParcelable(AppConst.PARC_OPERATION);
            if (operation != null) {
                populateTextView();
            }
        }

        return rooView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (PelangganFragmentInf) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context + " Activity must implement PelangganFragmentInf");
        }
    }

    @OnClick(R.id.btn_periksa) public void goPeriksa(View v) {
        Bundle args = new Bundle();
        args.putString(AppConst.TAG_TO_ID, operation.getId());
        args.putInt(AppConst.TAG_ITEM_ID, v.getId());
        mCallback.PelangganFragmentClickListener(args);
    }

    @OnClick(R.id.btn_check_onmap) public void checkOnMap() {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:<" + operation.getLatitude()  + ">,<"
                        + operation.getLongitude() + ">?q=<" + operation.getLatitude()  + ">,<"
                        + operation.getLongitude() + ">(" + operation.getAlamat() + ")"));
        startActivity(intent);
    }

    private void populateTextView() {
        tvIdPelanggan.setText(operation.getIdPelanggan());
        tvNamaPelanggan.setText(operation.getNama());
        tvAlamatPelanggan.setText(operation.getAlamat());
        tvDaya.setText(operation.getDaya());
        tvCt.setText(operation.getCtTerpasang());
        tvMerek.setText(operation.getMerek());
        tvGardu.setText(operation.getNoGardu());
        tvSeri.setText(operation.getSeri());
    }

}
