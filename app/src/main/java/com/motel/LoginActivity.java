package com.motel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.motel.apiresponse.LoginResponse;
import com.motel.helper.ApiEndPoint;
import com.motel.helper.AppConst;
import com.motel.helper.Utility;
import com.motel.model.User;

import java.io.IOException;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private ProgressDialog pDialog;
    private User mUser;

    private final String LOG_TAG = getClass().getSimpleName();
    private Context mContext;

    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_password)
    TextView tvPassword;

    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        mContext = this;

        ButterKnife.bind(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void checkLoginWithApi() {

        retrofit = Utility.getClient(username, password, this);

        pDialog.show();
        ApiEndPoint endPoint = retrofit.create(ApiEndPoint.class);
        Call<LoginResponse> call = endPoint.checkLoginApi();
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                pDialog.dismiss();
                if (response.code() == AppConst.HTTP_OK) {
                    if (response.body().isStatus()) {
                        mUser = response.body().getUser();

                        Utility.saveUserOnPrf(mContext, mUser);

                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        i.putExtra(AppConst.PARC_USER, mUser);
                        startActivity(i);
                        finish();

                    } else {
                        Utility.displayAlert(mContext, response.body().getMsg());
                        Log.d(LOG_TAG, "response status false");
                    }

                } else if(response.code() == AppConst.HTTP_UNAUTHORIZED) {
                    Utility.displayAlert(mContext, "Login gagal, username atau password salah.");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(mContext, t.getMessage());
                }
            }
        });
    }

    @OnClick(R.id.btn_login) public void goCheckLogin() {
        username = tvUsername.getText().toString();
        password = tvPassword.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Utility.displayAlert(mContext, "Field username dan password tidak boleh kosong");
            return;
        }
        checkLoginWithApi();
    }
}
